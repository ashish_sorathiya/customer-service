package in.motorzo.customerservice.authantications;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.motorzo.customerservice.MainActivity;
import in.motorzo.customerservice.R;
import in.motorzo.customerservice.activities.AddCar;
import in.motorzo.customerservice.database.UserDetailsPrefrences;
import in.motorzo.customerservice.network.VolleySingleton;
import in.motorzo.customerservice.utils.StringApiKeys;

public class SignInActivity extends AppCompatActivity {

    private Button signinButton,signupButton,skipButton;
    private EditText email,password;
    ProgressDialog dialog;
    private String requestUrl="http://52.76.80.19:8080/authservice/authentication/generatetoken";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        signinButton= (Button) findViewById(R.id.signin_button);
        signupButton= (Button) findViewById(R.id.signup_button);
        skipButton= (Button) findViewById(R.id.skip_button);
        email= (EditText) findViewById(R.id.email_text);
        password= (EditText) findViewById(R.id.password_text);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    loginRequest();

                Intent i=new Intent(SignInActivity.this,AddCar.class);
                startActivity(i);
            }
        });
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SignInActivity.this,SignUpActivity.class);
                startActivity(i);
            }
        });
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SignInActivity.this,MainActivity.class);
                startActivity(i);
            }
        });
    }

    public void loginRequest(){
        RequestQueue requestQueue= VolleySingleton.getInstance(this.getApplicationContext()).getRequestQueue();



        Map<String,String> parameters=new HashMap<String,String>();
        parameters.put(StringApiKeys.KEY_EMAIL,"sorathiya.ashish@motorzo.in");
        parameters.put(StringApiKeys.KEY_PASSWORD,password.getText().toString());
        parameters.put(StringApiKeys.KEY_LOGINFROM,"WEB");
        parameters.put(StringApiKeys.KEY_AUTHTYPE, "application");
        parameters.put(StringApiKeys.KEY_USERTYPE, "customer");

        JSONObject reqObject=new JSONObject(parameters);
        Toast.makeText(getApplicationContext(), "" + reqObject, Toast.LENGTH_LONG).show();


        JsonObjectRequest loginrequest=new JsonObjectRequest(Request.Method.POST, requestUrl,reqObject,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String msg="not signed in";
                try {
                   msg=response.getString(StringApiKeys.KEY_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(),"response : "+msg,Toast.LENGTH_LONG).show();
                parsRespondData(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"error message : "+error,Toast.LENGTH_LONG).show();
                Log.d("MZERROR", error.getStackTrace().toString());
            }
        }){


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> header=new HashMap<String,String>();
                header.put("Content-Type", "application/json");
                return header;
            }
        };

        requestQueue.add(loginrequest);
    }

    private void parsRespondData(JSONObject response)  {
        String u_name,u_email,u_mobileNo,u_authToken,u_id,u_state,u_city,u_dob;
        try {

            JSONObject authInfo=response.getJSONObject("data").getJSONObject("authToken");
            JSONObject userInfo=response.getJSONObject("data").getJSONObject("user");

            u_email=authInfo.getString(StringApiKeys.KEY_EMAIL);
            u_authToken=authInfo.getString(StringApiKeys.KEY_AUTHTOKEN);

            u_mobileNo=userInfo.getString(StringApiKeys.KEY_USERMOBILENO);
            u_name=userInfo.getString(StringApiKeys.KEY_USERNAME);
            u_id=userInfo.getString(StringApiKeys.KEY_USERID);
            u_city=userInfo.getString(StringApiKeys.KEY_USERCITY);
            u_state=userInfo.getString(StringApiKeys.KEY_USERSTATE);
            u_dob=userInfo.getString(StringApiKeys.KEY_USERDOB);

            Log.d("ROHITemail", u_email);
            Log.d("ROHITuID", u_id);
            Log.d("ROHITtoken", u_authToken);
            Log.d("ROHITmo", u_mobileNo);


           UserDetailsPrefrences userDetailsPrefrences=new UserDetailsPrefrences(getApplicationContext());

            userDetailsPrefrences.saveToUserDetails(StringApiKeys.KEY_EMAIL,u_email);
            userDetailsPrefrences.saveToUserDetails(StringApiKeys.KEY_AUTHTOKEN,u_authToken);
            userDetailsPrefrences.saveToUserDetails(StringApiKeys.KEY_USERNAME,u_name);
            userDetailsPrefrences.saveToUserDetails(StringApiKeys.KEY_USERMOBILENO,u_mobileNo);
            userDetailsPrefrences.saveToUserDetails(StringApiKeys.KEY_USERCITY,u_city);
            userDetailsPrefrences.saveToUserDetails(StringApiKeys.KEY_USERSTATE,u_state);
            userDetailsPrefrences.saveToUserDetails(StringApiKeys.KEY_USERDOB,u_dob);
            userDetailsPrefrences.saveToUserDetails(StringApiKeys.KEY_USERID,u_id);


        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(),"error msg : "+e.getMessage(),Toast.LENGTH_LONG).show();
        }


    }


}
