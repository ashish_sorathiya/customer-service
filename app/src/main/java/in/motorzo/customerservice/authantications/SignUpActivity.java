package in.motorzo.customerservice.authantications;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import in.motorzo.customerservice.MainActivity;
import in.motorzo.customerservice.R;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    private Button signUp,skip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        signUp= (Button) findViewById(R.id.signup_button_two);
        skip= (Button) findViewById(R.id.skip_button_two);
        signUp.setOnClickListener(this);
        skip.setOnClickListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        Intent i;
        if(v.getId()==R.id.signup_button_two){
            i=new Intent(SignUpActivity.this,MainActivity.class);
            startActivity(i);
        }
        if(v.getId()==R.id.skip_button_two){
            i=new Intent(SignUpActivity.this,MainActivity.class);
            startActivity(i);
        }
    }

}
