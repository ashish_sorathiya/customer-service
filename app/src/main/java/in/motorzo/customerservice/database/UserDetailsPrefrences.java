package in.motorzo.customerservice.database;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ashishsorathiya on 3/7/16.
 */
public class UserDetailsPrefrences {

    public SharedPreferences sharedPreferences;
    public Context sContext;
    public UserDetailsPrefrences(Context sContext){
        this.sContext=sContext;
    }



    public void saveToUserDetails(String key,String value){

        sharedPreferences=sContext.getSharedPreferences("userData",sContext.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(key,value);
        //editor.apply();
        editor.commit();


    }

}
