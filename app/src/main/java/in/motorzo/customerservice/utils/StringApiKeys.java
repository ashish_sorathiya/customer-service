package in.motorzo.customerservice.utils;

/**
 * Created by ashishsorathiya on 3/7/16.
 */
public class StringApiKeys {

    public static final String KEY_EMAIL="email";
    public static final String KEY_PASSWORD="password";
    public static final String KEY_LOGINFROM="loginFrom";
    public static final String KEY_AUTHTYPE="authType";
    public static final String KEY_USERTYPE="userType";

    public static final String KEY_MESSAGE="message";
    public static final String KEY_AUTHTOKEN="authToken";

    public static final String KEY_USERNAME="name";
    public static final String KEY_USERID="id";
    public static final String KEY_USERMOBILENO="mobileNumber";
    public static final String KEY_USERSTATE="state";
    public static final String KEY_USERCITY="city";
    public static final String KEY_USERDOB="dob";

    public static final String KEY_CAR_REGNO="registrationNo";
    public static final String KEY_CAR_MARKER="carMaker";
    public static final String KEY_CAR_MODEL="model";
    public static final String KEY_CAR_FUELTYPE="fuelType";
    public static final String KEY_CAR_MAKEYEAR="makeYear";
    public static final String KEY_CAR_MAKEMONTH="makeMonth";
    public static final String KEY_CAR_ODOMETER="odometer";

    public static final String VALUE_API_VERISON="1.1";



}
