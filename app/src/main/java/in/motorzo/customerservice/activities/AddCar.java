package in.motorzo.customerservice.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.motorzo.customerservice.MainActivity;
import in.motorzo.customerservice.R;
import in.motorzo.customerservice.modelclasses.MyCarDetails;
import in.motorzo.customerservice.network.VolleySingleton;
import in.motorzo.customerservice.utils.StringApiKeys;

public class AddCar extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener{
    private Button continue_button;
    private Spinner s_month,s_year;
    ArrayList<String> years = new ArrayList<String>();
    List<MyCarDetails> my_cars=new ArrayList<MyCarDetails>();
    private EditText c_regno,c_marker,c_model;
    String c_fuel_type;
    ProgressDialog progress;
    CheckBox chk_petrol,chk_diesel,chk_other;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1900; i <= 2017; i++) {
            years.add(Integer.toString(i));
        }

        continue_button= (Button) findViewById(R.id.add_and_continue_button);
        continue_button.setOnClickListener(this);

        s_month= (Spinner) findViewById(R.id.spinner_month);
        s_year= (Spinner) findViewById(R.id.spinner_year);

        c_regno= (EditText) findViewById(R.id.car_reg_no);
        c_marker= (EditText) findViewById(R.id.car_marker);
        c_model= (EditText) findViewById(R.id.car_model);

        chk_diesel= (CheckBox) findViewById(R.id.chk_diesel);
        chk_petrol= (CheckBox) findViewById(R.id.chk_petrol);
        chk_other= (CheckBox) findViewById(R.id.chk_other);
        chk_diesel.setOnClickListener(this);
        chk_petrol.setOnClickListener(this);
        chk_other.setOnClickListener(this);

        ArrayAdapter<CharSequence> adapter_month = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
        adapter_month.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_month.setAdapter(adapter_month);

        ArrayAdapter<String> adapter_year = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, years);
        s_year.setAdapter(adapter_year);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


    }

    public void carAddRequest(){

        String addCarUrl="http://52.76.80.19:8080/usermgmt/api/usercar";
        RequestQueue requestQueue= VolleySingleton.getInstance(this.getApplicationContext()).getRequestQueue();
        Map<String,String> parameters=new HashMap<String,String>();
        parameters.put(StringApiKeys.KEY_USERID, getSharedPreferences("userData", MODE_PRIVATE).getString(StringApiKeys.KEY_USERID, "non"));

        Toast.makeText(getApplicationContext(),""+getSharedPreferences("userData", MODE_PRIVATE).getString(StringApiKeys.KEY_USERID, "non"),Toast.LENGTH_LONG).show();
        parameters.put(StringApiKeys.KEY_CAR_REGNO,c_regno.getText().toString());
        parameters.put(StringApiKeys.KEY_CAR_MODEL,c_model.getText().toString());
        parameters.put(StringApiKeys.KEY_CAR_MARKER,c_marker.getText().toString());
        parameters.put(StringApiKeys.KEY_CAR_FUELTYPE, c_fuel_type);
        parameters.put(StringApiKeys.KEY_CAR_MAKEYEAR, "2010");
        parameters.put(StringApiKeys.KEY_CAR_MAKEMONTH, "3");
        parameters.put(StringApiKeys.KEY_CAR_ODOMETER, "1200");

        JsonObjectRequest addcarRequest=new JsonObjectRequest(Request.Method.POST, addCarUrl, new JSONObject(parameters), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String msg="not added";
                try {
                    msg=response.getString(StringApiKeys.KEY_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext()," added : "+msg,Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext()," respond : "+response,Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext()," error in add car request : "+error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String,String> header=new HashMap<String,String>();
                header.put("Content-Type", "application/json");
                //String res = getSharedPreferences("userData",MODE_PRIVATE).getString(StringApiKeys.KEY_AUTHTOKEN,"none");
                //Log.w("ROHIT", res);

                header.put("authToken", getSharedPreferences("userData",MODE_PRIVATE).getString(StringApiKeys.KEY_AUTHTOKEN,"none"));
                header.put("apiVersion",StringApiKeys.VALUE_API_VERISON);
                header.put("userType","customer" );
                header.put("loginFrom", "web");



                return header;
            }
        };
        requestQueue.add(addcarRequest);
    }



    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.add_and_continue_button){

            carAddRequest();

            Intent i =new Intent(AddCar.this, MainActivity.class);
            startActivity(i);
        }
        if(v.getId()==R.id.chk_petrol){
            c_fuel_type="petrol";
        }
        if(v.getId()==R.id.chk_diesel){
            c_fuel_type="diesel";
        }
        if(v.getId()==R.id.chk_other){
            c_fuel_type="other";
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
