package in.motorzo.customerservice.modelclasses;

import in.motorzo.customerservice.utils.StringApiKeys;

/**
 * Created by ashishsorathiya on 3/7/16.
 */
public class MyCarDetails {
    private String c_model,c_marker,c_fuel_type,c_odometer_reading;
    public MyCarDetails(String c_marker,String c_model,String c_fuel_type,String c_odometer_reading){
        this.c_marker=c_marker;
        this.c_model=c_model;
        this.c_fuel_type=c_fuel_type;
        this.c_odometer_reading=c_odometer_reading;
    }

    public String getC_fuel_type() {
        return c_fuel_type;
    }

    public String getC_marker() {
        return c_marker;
    }

    public String getC_model() {
        return c_model;
    }

    public String getC_odometer_reading() {
        return c_odometer_reading;
    }
}
