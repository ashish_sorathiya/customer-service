package in.motorzo.customerservice.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import in.motorzo.customerservice.fragments.FragmemtRegulerService;
import in.motorzo.customerservice.fragments.FragmentCarRepair;
import in.motorzo.customerservice.fragments.FragmentDentPaint;

/**
 * Created by ashishsorathiya on 3/6/16.
 */
public class BottomPagerAdapter extends FragmentPagerAdapter{
    private String tabTitles[] = new String[] { "Tab1", "Tab2", "Tab3" };
    public BottomPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f;
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                f=new FragmemtRegulerService();
                return f;
            case 1: // Fragment # 0 - This will show FirstFragment different title
                f=new FragmentCarRepair();
                return f;
            case 2: // Fragment # 1 - This will show SecondFragment
                f=new FragmentDentPaint();
                return f;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 3;
    }


}

