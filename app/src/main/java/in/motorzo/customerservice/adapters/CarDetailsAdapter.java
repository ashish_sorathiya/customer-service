package in.motorzo.customerservice.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by ashishsorathiya on 3/7/16.
 */
public class CarDetailsAdapter extends RecyclerView.Adapter<CarDetailsAdapter.CarDetailsViewholder> {

    public CarDetailsAdapter(Context context){

    }

    @Override
    public CarDetailsViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(CarDetailsViewholder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class CarDetailsViewholder extends RecyclerView.ViewHolder{

        public CarDetailsViewholder(View itemView) {
            super(itemView);
        }
    }
}
