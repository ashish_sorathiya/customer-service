package in.motorzo.customerservice.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import in.motorzo.customerservice.R;

/**
 * Created by ashishsorathiya on 3/6/16.
 */
public class TopPagerAdapter extends PagerAdapter{

    private int[] images={R.drawable.garage,R.drawable.garage,R.drawable.garage};
    Context mContext;
    LayoutInflater mLayoutInflater;
    public TopPagerAdapter(Context context){
        mContext=context;
        mLayoutInflater=LayoutInflater.from(context);
    }



    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_pager_layout, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.pager_image);
        imageView.setImageResource(images[position]);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
